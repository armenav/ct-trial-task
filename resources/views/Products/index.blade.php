@extends('layouts.master')

@section('content')

    <div class="container">

        <form id="productForm" method="POST" action="">

            <div class="form-group">
                <label for="name">Name:</label>
                <input type="text" class="form-control" id="name" name="name">
            </div>

            <div class="form-group">
                <label for="quantity">Quantity in stock:</label>
                <input type="text" class="form-control" id="quantity" name="quantity">
            </div>

            <div class="form-group">
                <label for="price">Price per item:</label>
                <input type="text" class="form-control" id="price" name="price">
            </div>


            <div class="form-group">
                <button id="save" class="btn btn-primary" type="submit">Save</button>
            </div>

        </form>

        <table id="productList" class="table">
            <thead>
            <tr>
                <th>Product</th>
                <th>Quantity in stock</th>
                <th>Price per item</th>
                <th>Datetime submitted</th>
                <th>Total value number</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($productList as $product)
                <tr id="{{$product->id}}">
                    <td>{{$product->name}}</td>
                    <td>{{$product->quantity}}</td>
                    <td>{{$product->price}}</td>
                    <td>{{$product->dateTime}}</td>
                    <td>{{$product->totalPrice}}</td>
                    <td>
                        <button class="btn btn-link rowEdit" type="button" >Edit</button>
                    </td>
                </tr>
            @endforeach
            <tr>
                <td colspan="4">Total Sum</td>
                <td id="totalSum">{{$totalSum}}</td>
                <td></td>
            </tr>
            </tbody>
        </table>


    </div>

@endsection

