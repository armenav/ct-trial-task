# CT product management

### Getting Started

- Run composer to install dependencies
- This Application uses local storage and stores data in storage/app/products.json
- Application is ready to go, just visit the home directory
- enjoy!

### Things that can be improved

- Write tests
