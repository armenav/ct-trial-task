<?php

namespace App\Models;

use Carbon\Carbon;

/**
 * Class Product
 * @package App\Models
 */
class Product {

    /**
     * @var string
     */
    public $id;
    /**
     * @var string
     */
    public $dateTime;
    /**
     * @var string
     */
    public $name;
    /**
     * @var integer
     */
    public $quantity;
    /**
     * @var integer
     */
    public $price;
    /**
     * @var integer
     */
    public $totalPrice;

    /**
     * Product constructor.
     */
    public function __construct() {
        $this->id = str_random(40);
        $this->dateTime = Carbon::now()->toDateTimeString();
    }
}
