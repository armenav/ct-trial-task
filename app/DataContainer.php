<?php

namespace App;


use Illuminate\Support\Facades\Storage;

/**
 * Class DataContainer
 * @package App
 */
class DataContainer
{

	/**
	 * @var string
	 */
	protected $filename;

	/**
	 * @var Storage
	 */
	protected $localContainer;

	/**
	 * DataContainer constructor.
	 *
	 * @param string $fileName
	 */
	public function __construct($fileName = "products.json") {

        $this->filename = $fileName;

        $this->localContainer = Storage::disk('local');

        if (!$this->fileContainerExists()){
            $this->createFileContainer();
        };

    }

	/**
	 * @return bool
	 */
	protected function fileContainerExists():bool
    {

        return $this->localContainer->exists($this->filename);

    }

	protected function createFileContainer(){

        $this->localContainer->put($this->filename, '{}');

    }

	/**
	 * @param array $list
	 */
	public function saveData(array $list){

        $this->localContainer->put($this->filename, json_encode($list));

    }


	/**
	 * @return array
	 */
	public function loadData():array
    {

        $list = json_decode($this->localContainer->get($this->filename));

        return (array)$list;

    }

}
