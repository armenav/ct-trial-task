<?php

namespace App\Repositories;


use App\DataContainer;
use App\Models\Product;

class ProductRepository
{
	/**
	 * @var DataContainer
	 */
    protected $dataContainer;

	/**
	 * ProductRepository constructor.
	 *
	 * @param DataContainer $dataContainer
	 */
    public function __construct(DataContainer $dataContainer)
    {
        $this->dataContainer = $dataContainer;
        $this->products = $this->dataContainer->loadData();
    }

	/**
	 * @return array
	 */
    public function getProducts()
    {
        return $this->dataContainer->loadData();
    }

	/**
	 * @return int
	 */
    public function calculateSum():int
    {
        $data = $this->dataContainer->loadData();
        if (sizeof($data) > 0) {
            return array_reduce($data, function ($sum, $product) {
                return $sum += $product->totalPrice;
            });
        } else {
            return 0;
        }

    }

	/**
	 * @param Product $product
	 */
    public function addData(Product $product)
    {

        $productList = $this->getProducts();

        // Todo: check for duplicate id
        $productList[$product->id] = $product;

        $this->dataContainer->saveData($productList);

    }

}
