<?php

namespace App\Http\Controllers;

use App\DataContainer;
use App\Models\Product;
use App\Repositories\ProductRepository;

use Illuminate\Http\Request;

class ProductsController extends Controller
{

	/**
	 * @var ProductRepository
	 */
	protected $productRepository;

	/**
	 * ProductsController constructor.
	 */
	public function __construct() {

        $this->productRepository = new ProductRepository(new DataContainer());

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $productList = $this->productRepository->getProducts();

        $totalSum = $this->productRepository->calculateSum();

        return view('products.index', compact(['productList', 'totalSum']));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate(request(), [
            'name' => 'required|max:255',
            'quantity'  => 'required|numeric|min:1',
            'price'  => 'required|numeric|min:0',
        ]);

        $product = (new Product());

        $product->name = request('name');
        $product->quantity = request('quantity');
        $product->price = request('price');
        $product->totalPrice = $product->quantity * $product->price;

        $data = [
            "success" => true,
            "msg"     => "",
            "product" => $product,
        ];

	    try {
		    $this->productRepository->addData( $product );
	    } catch ( \Throwable $t ) {
		    $data["success"]  = false;
		    $data["msg"]      = $t->getMessage();
		    $data["product"]  = null;
		    $data["totalSum"] = null;
	    }

	    $data["totalSum"] = $this->productRepository->calculateSum();

        return $data;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

}
