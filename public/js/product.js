$(function () {

    $('#productForm').on('submit', function (e) {

        if (!$('#productForm').valid()) {
            return;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        e.preventDefault(e);

        $.ajax({

            type: "POST",
            url: '/',
            data: $(this).serialize(),
            dataType: 'json',
            success: function (data) {

                if (data['success'] == true) {

                    var product = data['product'];
                    var totalSum = data['totalSum'];

                    var newRow = $('<tr id="' + product["id"] + '">' +
                        '<td>' + product["name"] + '</td>' +
                        '<td>' + product["quantity"] + '</td>' +
                        '<td>' + product["price"] + '</td>' +
                        '<td>' + product["dateTime"] + '</td>' +
                        '<td>' + product["totalPrice"] + '</td>' +
                        '<td><button class="btn btn-link rowEdit" type="button" >Edit</button></td>' +
                        '</tr>');
                    $('#productList tr:last').before(newRow);

                    $('#totalSum').text(totalSum);

                } else {

                    alert(data['msg']);
                }
            },
            error: function (data) {
                var errors = data.responseJSON;

                var array = $.map(errors, function (value, index) {
                    return [value[0]];
                });
                alert("Server could not process your request");
            }
        })
    });

    $(document).on('click', '#productList .rowEdit', function (e) {

        var id = e.target.parentNode.parentNode.id;

        alert("You want to edit product with id: " + id);

    });


    $('#productForm').validate({ // initialize the plugin
        rules: {
            name: {
                required: true,
                maxlength: 255
            },
            quantity: {
                required: true,
                number: true,
                min: 1
            },
            price: {
                required: true,
                number: true,
                min: 0
            }
        },
        errorClass: "js-error",
    });

});
